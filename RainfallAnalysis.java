/*
 * Dany Turcios
 * 10/1/2015
 * Rain Analysis Program
 * This program will first ask how many days of rainfall you wish to record and then record your input for each day
 * It will then bring up a menu to display the results in various ways.
 */
import java.util.Scanner;
public class RainAnalysis {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
        //State variables here
		int day=1, i=0, choice, maxDay=0, minDay=0;
		double sum=0, avgRain=0, rain, minRain= Integer.MAX_VALUE, maxRain=Integer.MIN_VALUE;
        //Begin input for amount of days monitored
		System.out.println("How many days do you want to monitor rainfall? (Must be between 4-10)");
		int n = keyboard.nextInt();
        //Loop statement starts here
		while(i != n){
            //Only numbers 4-10
			if (n <= 10 && n  >= 4){
				System.out.println("Enter the rainfall for day "+day+":");
					rain = keyboard.nextDouble();
                    //Sum and AvgRain are established here and these will be used later on within menu
					sum = sum + rain;
					avgRain = sum/n;
                    //This records the first maximum and minimum rainfall since it is </> not <=/>=.
					if (rain<minRain){
						minRain=rain;
						minDay=day;
			}
					else if (rain>maxRain){
						maxRain=rain;
						maxDay=day;
			}
                    //This is where variables day and i increase by 1
					day = day + 1;
					i = i + 1;
				}
			else{
				System.out.println("Invalid Response, Please try again!");
				System.out.println("\n How many days do you want to monitor rainfall? (Must be between 4-10)");
				n = keyboard.nextInt();
			}	
		}
// End of recording rainfall section/ Beginning of Menu Section//////////////////////////////////////////////////////////////////////////////////////////////////
		String menu = " \n Rainfall Data Display Menu\n 1 - Display total rainfall\n 2 - Display rain average for the period\n 3 - Display if drought flood or normal conditions\n 4 - Display the day and amount of the highest rainfall\n 5 - Display the day and amount of the lowest rainfall\n 6 - Quit the program\n \n Input your choice:";
        //first display of menu
		System.out.println(menu);
		choice = keyboard.nextInt();
        //begin nested if statement
		while(choice != 6){
			if (choice == 1){
				System.out.println("The total rainfall was "+sum);
		}
			else if (choice == 2){
				System.out.printf("The rain average for the period is %.2f",avgRain);
		}
			else if (choice == 3){
                //if statement within choice 3 begins
				if (avgRain <= 0){
					System.out.println("It is drought.");
			}
				else if (avgRain >= 3){
					System.out.println("It is flooding.");
			}
				else{
					System.out.println("It is normal conditions.");
			}
		}
        //End of choice 3 if statement/ Start of choice 4
			else if (choice == 4){
				System.out.println("Day "+maxDay+" had "+maxRain+" inches.");
		}
			else if (choice == 5){
				System.out.println("Day "+minDay+" had "+minRain+" inches.");
		}
			else if (choice == 6){
				//exits program
		}
			else{
				System.out.println("\nInvalid choice, please choose a valid number.");
		}
			//Reintroduces Menu
			System.out.print(menu);
			choice = keyboard.nextInt();
		}
        // End of program
		System.out.println("\nThank you! Goodbye.");
	}

}
